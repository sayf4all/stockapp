const { EventHubClient, delay } = require("@azure/event-hubs");

const connectionString = "Endpoint=sb://sayfhubs.servicebus.windows.net/;SharedAccessKeyName=sayfEventHubsSharedKey;SharedAccessKey=fgyTyuaV3lF14kIpOHkYwCJIW1f6uj3M5DRz9F2RKKw=";
const eventHubsName = "sayfeventhub";

async function main() {
    const client = EventHubClient.createFromConnectionString(connectionString, eventHubsName);
    const allPartitionIds = await client.getPartitionIds();
    const firstPartitionId = allPartitionIds[0];

    const receiveHandler = client.receive(firstPartitionId, eventData => {
        console.log(`${JSON.stringify(eventData.body)}`);
    }, error => {
        console.log('Error when receiving message: ', error)
    });

    // Sleep for a while before stopping the receive operation.
    await delay(15000);
    await receiveHandler.stop();
    await client.close();
}

main().catch(err => {
    console.log("Error occurred: ", err);
});