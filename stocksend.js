const {EventHubClient, EventPosition} = require('@azure/event-hubs');

// Manhal event hub
//let connectionString = "Endpoint=sb://sayedxeh.servicebus.windows.net/;SharedAccessKeyName=SandLRights;SharedAccessKey=3fk6gx6gvSYF+Bp3v624fCYB9GxSC+iVhUPaBX/ghTo=;EntityPath=sayedxeventhub";
//let eventHub = "sayedxeventhub";

const connectionString = "Endpoint=sb://sayfhubs.servicebus.windows.net/;SharedAccessKeyName=sayfEventHubsSharedKey;SharedAccessKey=fgyTyuaV3lF14kIpOHkYwCJIW1f6uj3M5DRz9F2RKKw=";
const eventHub = "sayfeventhub";

const client = EventHubClient.createFromConnectionString(connectionString, eventHub);

var t = 1;
var interval = 5000;

function getAggergatedData(winstart, winend, storeid, productid, netadjustment, avgadjustment, adjustments) {
    return {
        body: {
            winstart: winstart,
            winend: winend,
            storeid: storeid,
            productid: productid,
            netadjustment: netadjustment,
            avgadjustment: avgadjustment,
            adjustments: adjustments
        }
    };
}

function getCurrentTimeInSeconds() {
    return new Date().getTime() / 1000;
}

function getEndTimeInSeconds(winstart) {
    return (winstart.getTime() / 1000) + 60;
}

function getAgergateDataArray(statisticsMap, winstart, winend) {
    var aggergatedDataArray = [];
    for (var keyPair of statisticsMap.keys()) {
        var adjustmentsArray = statisticsMap.get(keyPair);

        var avgAdjustment = 0;
        var netAdjustment = 0;
        var adjustmentsAantal = adjustmentsArray.length;

        for (i = 0; i < adjustmentsArray.length; i++) {
            netAdjustment = netAdjustment + adjustmentsArray[i].body.StockAdjust;
        }
        avgAdjustment = netAdjustment / adjustmentsAantal;
        let storeid = adjustmentsArray[0].body.StoreID;
        let productid = adjustmentsArray[0].body.ProductID;
        aggergatedDataArray.push(getAggergatedData(winstart, winend, storeid, productid, netAdjustment, avgAdjustment, adjustmentsAantal));
    }
    return aggergatedDataArray;
}


function sendAggregateData(client, aggergatedDataArray) {
    for (var aggergatedData of aggergatedDataArray) {
        var aggergatedDataMessage = getAggergatedData(aggergatedData.body.winstart,
            aggergatedData.body.winend,
            aggergatedData.body.storeid,
            aggergatedData.body.productid,
            aggergatedData.body.netadjustment,
            aggergatedData.body.avgadjustment,
            aggergatedData.body.adjustments);
        console.log(`Sending aggeregate data: ${JSON.stringify(aggergatedDataMessage.body)}`);
        client.send(aggergatedDataMessage);
    }
}

let stockAdjustments = [];
let winstart = new Date();


setInterval(function () {
    var d = new Date();
    var us = 308;
    var a = us + (d.getSeconds() % 3);
    var sID = Math.floor(Math.random() * (310 + us) - us);
    var adjst = Math.floor(Math.random() * (5 - 6) - 6);
    for (m = 1; m <= Math.floor((Math.sqrt(t / 60000)) + 1); m++) {
        var p = Math.floor(Math.random() * ((((m % 3) + 1) * 10) - ((m % 3) + 1) + 1) + ((m % 3) + 1));
        var pID = p * Math.random() + 1;
        for (i = us; i <= a; i++) {
            console.log("Receiving data...")
            var sa = Math.floor(((i % 3) + 1) * Math.sign(Math.random() * (2) - 3));
            if (sa == 0) {
                sa = -1;
            }
            ;
            //client.send({body: {Date: d.toISOString(), StoreID: i, ProductID: p, StockAdjust: sa}});
            //toegevoegd extra functionaliteit
            stockAdjustments.push({body: {Date: d.toISOString(), StoreID: i, ProductID: p, StockAdjust: sa}});
            if (getCurrentTimeInSeconds() >= getEndTimeInSeconds(winstart)) {
                var winend = new Date();
                var statisticsMap = new Map();

                for (let i = 0; i < stockAdjustments.length; i++) {
                    var adjustment = stockAdjustments[i];
                    var key = adjustment.body.StoreID + "," + adjustment.body.ProductID;

                    if (statisticsMap.get(key) != undefined) {
                        statisticsMap.get(key).push(adjustment);
                    } else {
                        var newAdjustmentArray = [];
                        newAdjustmentArray.push(adjustment);
                        statisticsMap.set(key, newAdjustmentArray);
                    }
                }
                //iterate over collected data pairs
                var aggergatedDataArray = getAgergateDataArray(statisticsMap, winstart.toISOString(), winend.toISOString());

                //verstuur aggeregateData
                sendAggregateData(client, aggergatedDataArray, winstart, winend);

                //reset window en aggergate gegevens
                winstart = new Date();
                stockAdjustments = [];

            }

        }
    }
    t = t + interval;
    runtime = t / 60000;
    console.log(runtime.toFixed(2) + " minutes elapsed.");
}, interval);
