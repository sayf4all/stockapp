const {EventHubClient} = require("@azure/event-hubs");

// Connection string - primary key of the Event Hubs namespace.
// For example: Endpoint=sb://myeventhubns.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//sayf const connectionString = "Endpoint=sb://sayfhubs.servicebus.windows.net/;SharedAccessKeyName=sayfEventHubsSharedKey;SharedAccessKey=fgyTyuaV3lF14kIpOHkYwCJIW1f6uj3M5DRz9F2RKKw=";
const connectionString = "Endpoint=sb://tamedxeh.servicebus.windows.net/;SharedAccessKeyName=tamedxAcccessLS;SharedAccessKey=1tr3ikarsvK6ENLd2fNMNxSzeZvWYbPj90sLmZtMuGQ=;EntityPath=tamedxeventhub";


// Name of the event hub. For example: myeventhub
//const eventHubsName = "sayfeventhub";
const eventHubsName = "tamedxeventhub";


function pauseForRandomTime() {
    var millis = Math.floor((Math.random() * 500));
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    }
    while (curDate - date < millis);
}

function getStroeId() {
    return 308 + Math.floor(Math.random() * 3);
}

function getStockAdjustment() {
    let stockAdjustment = (Math.floor(Math.random() * 10)) - (Math.floor(Math.random() * 10));
    if (stockAdjustment != 0) {
        return stockAdjustment;
    }
    return 1;
}

function getProductID() {
    return 1 + Math.floor(Math.random() * 3);
}

function getJsonDate() {
    return new Date().toJSON();
}

function getAggergatedData(winstart, winend, storeid, productid, netadjustment, avgadjustment, adjustments) {
    return {
        body: {
            winstart: winstart,
            winend: winend,
            storeid: storeid,
            productid: productid,
            netadjustment: netadjustment,
            avgadjustment: avgadjustment,
            adjustments: adjustments
        }
    };
}

function getCurrentTimeInSeconds() {
    return new Date().getTime() / 1000;
}

function getEndTimeInSeconds(winstart) {
    return (winstart.getTime() / 1000) + 60;
}

async function main() {
    const client = EventHubClient.createFromConnectionString(connectionString, eventHubsName);
    let stockAdjustments = [];
    let winstart = new Date();


    while (true) {
        let adjustmentMessage = {
            body: {
                Date: getJsonDate(),
                StoreID: getStroeId(),
                ProductID: getProductID(),
                StockAdjust: getStockAdjustment()
            }
        };
        stockAdjustments.push(adjustmentMessage);
        console.log(`Sending message: ${JSON.stringify(adjustmentMessage.body)}`);
        await client.send(adjustmentMessage);
        if (getCurrentTimeInSeconds() >= getEndTimeInSeconds(winstart)) {
            var winend = new Date();
            var statisticsMap = new Map();

            for (let i = 0; i < stockAdjustments.length; i++) {
                var adjustment = stockAdjustments[i];
                var key = adjustment.body.StoreID + "," + adjustment.body.ProductID;

                if (statisticsMap.get(key) != null) {
                    statisticsMap.get(key).push(adjustment);
                } else {
                    var adjustmentArray = [];
                    adjustmentArray.push(adjustment);
                    statisticsMap.set(key, adjustmentArray);
                }
            }
            //iterate over collected data pairs
            var aggergatedDataArray = [];
            for (var keyPair of statisticsMap.keys()) {
                let adjustmentsArray = statisticsMap.get(keyPair);

                var avgAdjustment = 0;
                var netAdjustment = 0;
                var adjustmentsAantal = adjustmentsArray.length;

                for (i = 0; i < adjustmentArray.length; i++) {
                    netAdjustment = netAdjustment + adjustmentArray[i].body.StockAdjust;
                }
                avgAdjustment = netAdjustment / adjustmentsAantal;
                let storeid = adjustmentArray[0].body.StoreID;
                let productid = adjustmentArray[0].body.ProductID;
                aggergatedDataArray.push(getAggergatedData(winstart, winend, storeid, productid, netAdjustment, avgAdjustment, adjustmentsAantal));
            }
            //verstuur aggeregateData
            console.log(`Sending aggeregate data: winstart= ${winstart.toJSON()}`);
            for (var aggergatedData of aggergatedDataArray) {
                var aggergatedDataMessage = getAggergatedData(aggergatedData.body.winstart,
                    aggergatedData.body.winend,
                    aggergatedData.body.storeid,
                    aggergatedData.body.productid,
                    aggergatedData.body.netadjustment,
                    aggergatedData.body.avgadjustment,
                    aggergatedData.body.adjustments);
                console.log(`Sending aggeregate data: ${JSON.stringify(aggergatedDataMessage.body)}`);
                await client.send(aggergatedDataMessage);
            }
            console.log(`Sending aggeregate data: winend= ${winend.toJSON()}`);

            //reset window en aggergate gegevens
            winstart = new Date();
            stockAdjustments = [];

        }
    }
    await client.close();
}

main().catch(err => {
    console.log("Error occurred: ", err);
});